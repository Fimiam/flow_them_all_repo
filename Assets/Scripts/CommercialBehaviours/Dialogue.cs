using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogue : MonoBehaviour
{
    [SerializeField] private new Animation animation;

    [SerializeField] private TextMeshProUGUI text;

    [SerializeField] private Transform transformTarget;

    [SerializeField] private Vector3 offset;

    [SerializeField] private float smooth;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, transformTarget.position + offset, smooth);   
    }

    public void Hide()
    {
        animation.Play("dialogueHide");
    }

    public void Show()
    {
        animation.Play("dialogueShow");
    }

    public void SetDialogue(string text)
    {
        StartCoroutine(Setting(text));
    }

    private IEnumerator Setting(string text)
    {
        Hide();

        yield return new WaitUntil(() => !animation.isPlaying);

        this.text.text = text;

        Show();
    }
}
