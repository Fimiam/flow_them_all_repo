using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man : MonoBehaviour
{
    [SerializeField] private Dialogue dialogue;

    void Start()
    {
        StartCoroutine(DialogueProcess());
    }

    void Update()
    {
        
    }

    private IEnumerator DialogueProcess()
    {
        yield return new WaitForSeconds(1);

        dialogue.Show();

        yield return new WaitForSeconds(2f);

        dialogue.SetDialogue("HELP !!!");

        yield return new WaitForSeconds(2);

        dialogue.SetDialogue("PLEASE !!");

        yield return new WaitForSeconds(1);

        dialogue.Hide();
    }
}
