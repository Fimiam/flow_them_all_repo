using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicopter : MonoBehaviour
{
    private Vector3 dropPoint;

    
    void Start()
    {
        var container = FindObjectOfType<HelicopterPointsContainer>();

        dropPoint = container.GetDropPoint();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
