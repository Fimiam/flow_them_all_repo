using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;
using System;

public class VibrationManager : MonoBehaviour
{
    public static VibrationManager instance;

    [SerializeField] private float androidVibroDelay = .2f;

    private float lastVibro;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void VibroKill()
    {
        if (!Game.instance.vibroActive) return;

#if UNITY_ANDROID
        if (Time.time < lastVibro + androidVibroDelay) return;
#endif

        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    public void VibroMergeFail()
    {
        if (!Game.instance.vibroActive) return;

        //MMVibrationManager.Haptic(HapticTypes.Warning);
    }

    public void VibroLevelEnd()
    {
        if (!Game.instance.vibroActive) return;

        //MMVibrationManager.Haptic(HapticTypes.Success);
    }

    public void VibroClick()
    {
        if (!Game.instance.vibroActive) return;

        //MMVibrationManager.Haptic(HapticTypes.Selection);
    }

    internal void HummerHit()
    {
        if (!Game.instance.vibroActive) return;

        //MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }

    internal void VibroCompleteDestroy()
    {
        if (!Game.instance.vibroActive) return;

        //MMVibrationManager.Haptic(HapticTypes.SoftImpact);
    }
}
