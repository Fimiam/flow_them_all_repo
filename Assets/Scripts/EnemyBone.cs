using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBone : MonoBehaviour, IFlowInteractable
{
    private new Rigidbody rigidbody;

    private Enemy enemy;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        enemy = transform.GetComponentInParent<Enemy>();
    }

    public void CubeHit(float damage)
    {
        enemy.GetHit(damage);
    }

    public void FlowHit(float hitPercent, Vector3 hitDirection, Vector3 hitPosition)
    {
        rigidbody.AddForce(hitDirection * 25, ForceMode.Impulse);

        enemy.GetHit(.65f);
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void Interact(Waterflow flow)
    {
        
    }

    public bool IsFlowReflective()
    {
        return false;
    }
}
