using UnityEditor;
using UnityEngine;

[ExecuteAlways]
public class ColliderRenderer : MonoBehaviour
{
    [SerializeField] PolygonCollider2D _collider;

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        for (int p = 0; p < _collider.pathCount; p++)
        {
            for (int i = 0; i < _collider.GetPath(p).Length; i++)
            {
                Handles.Label(_collider.transform.TransformPoint(_collider.GetPath(p)[i]), i.ToString());

                if(i < _collider.GetPath(p).Length - 1)
                {
                    Debug.DrawLine(_collider.transform.TransformPoint(_collider.GetPath(p)[i]),
                        _collider.transform.TransformPoint(_collider.GetPath(p)[i + 1]));
                }
                else
                {
                    Debug.DrawLine(_collider.transform.TransformPoint(_collider.GetPath(p)[i]),
                        _collider.transform.TransformPoint(_collider.GetPath(p)[0]));
                }
            }
        }
    }

#endif

}
