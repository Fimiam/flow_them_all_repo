using RagdollMecanimMixer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class Enemy : MonoBehaviour
{
    [SerializeField] private HUD hud;

    [SerializeField] private Renderer renderer;

    [SerializeField] private RamecanMixer remecan;

    [SerializeField] private Animator animator;

    [SerializeField] private NavMeshAgent agent;


    [SerializeField] private float maxRunVelocity = 1;

    [SerializeField] private float runnigSpeed = 2;

    [SerializeField] private float Health;

    //[SerializeField] private List<GameObject> bones;

    private float maxHealth;

    public bool isDead;

    private int move_hash;

    public Cash2D cash_prefab;

    public AudioSource deathSource;

    [SerializeField] Rigidbody posterPoint;

    [SerializeField] private HandsIK handsIK;

    public GameObject torch;

    private void Awake()
    {
        maxHealth = Health;

        move_hash = Animator.StringToHash("movement");

        var p = transform.GetComponentInChildren<Poster>();

        if(p)
        {
            p.joint.connectedBody = posterPoint;

            handsIK.lHandTarget = p.leftHoldPoint;
            handsIK.rHandTarget = p.rightHoldPoint;

            handsIK.weight = 1.0f;

        }

        if(Random.value > .5f)
            torch.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        var character = FindObjectOfType<Player>();

        var targetPos = character.transform.position;

        Debug.DrawLine(transform.position, targetPos, Color.red, 4);

        agent.SetDestination(targetPos);

        agent.speed = runnigSpeed * Random.Range(.5f, 1);
    }

    void Update()
    {
        animator.SetFloat(move_hash, agent.velocity.magnitude / maxRunVelocity);
    }

    public void GetHit(float damage)
    {
        if (isDead) return;

        Health -= damage;

        //hud.SetHealth(Health / maxHealth);

        if(Health < 0)
        {
            isDead = true;

            agent.enabled = false;

            animator.enabled = false;

            remecan.BeginStateTransition("dead");

            var mat = renderer.material;

            mat.color = Color.blue;

            foreach (Transform item in transform.GetChild(0))
            {
                item.gameObject.layer = LayerMask.NameToLayer("Default");
            }

            Gameplay.Instance.EnemyKilled();

            VibrationManager.instance.VibroKill();

            var cashCount = Random.Range(2, 4);

            for (int i = 0; i < cashCount; i++)
            {
                var cash = Instantiate(cash_prefab);

                cash.transform.position = transform.position + Vector3.up * .6f;

                //cash.body.AddForce(Random.insideUnitSphere * 3, ForceMode.Impulse);

                //cash.body.AddTorque(Random.insideUnitSphere * 88, ForceMode.Impulse);

                var rPos = Random.insideUnitSphere;

                rPos.y = Mathf.Abs(rPos.y) * 2;

                cash.transform.DOMove(cash.transform.position + rPos, .3f);

                //cash.transform.DOScale(1, .5f);
            }

            if(Random.value > .5f)
                Gameplay.Instance.AddBearProgress(1);

            deathSource.Play();
        }
    }

    internal void CubeHit(float damage)
    {
        throw new System.NotImplementedException();
    }
}
