using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyCube : MonoBehaviour
{
    [SerializeField] private float damage = 11;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.TryGetComponent<EnemyBone>(out EnemyBone bone))
        {
            //Debug.Log("cube hit bone");

            bone.CubeHit(damage);
        }
    }
}
