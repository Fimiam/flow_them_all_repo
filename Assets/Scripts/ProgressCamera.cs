using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressCamera : MonoBehaviour
{
    private RenderTexture m_Texture;

    private Camera m_Camera;

    public string nameToSave = "level_1";

    public Texture2D textureToCompare;

    private Color[] targetColors;

    private bool[] targetPixels;

    private int pixelsCount;

    private int targetPixelsCount, initialTargetPixelCount, toComparePixels;

    private float initialPercent;

    public bool readyToCalculate;

    public float successProgress;

    public float targetExistPercent = 1f;

    private void Awake()
    {
        m_Camera = GetComponent<Camera>();
        m_Texture = m_Camera.targetTexture;

        //targetColors = textureToCompare.GetPixels(0, 0, textureToCompare.width, textureToCompare.height);
    }

    public void BeginCalculation()
    {
        targetColors = textureToCompare.GetPixels(0, 0, textureToCompare.width, textureToCompare.height);
        StartCoroutine(CalculateInitialPercent());
    }

    //private void Start()
    //{
    //    StartCoroutine(CalculateInitialPercent());
    //}

    private IEnumerator CalculateInitialPercent()
    {
        for (int i = 0; i < 6; i++)
        {
            yield return null;
        }

        //var currentTex = GetCurrentTexture();

        //var compareColors = currentTex.GetPixels(0, 0, currentTex.width, currentTex.height);

        pixelsCount = targetColors.Length;

        targetPixels = new bool[pixelsCount];

        for (int i = 0; i < targetColors.Length; i++)
        {
            if (targetColors[i].a > 0)
            {
                targetPixels[i] = true;
                targetPixelsCount++;
            }
            else
            {
                toComparePixels++;
            }
        }

        initialTargetPixelCount = targetPixelsCount;

        initialPercent = GetCompareResult();

        yield return null;

        readyToCalculate = true;
    }

    private int CalculateTextureDifference(Color[] toCompare)
    {
        int same = 0;

        for (int i = 0; i < pixelsCount; i++)
        {
            var color1 = targetColors[i];

            var color2 = toCompare[i];

            if (targetPixels[i])
            {
                if (toCompare[i].a < .5f)
                {
                    targetPixels[i] = false;
                    targetPixelsCount--;
                }
            }
            else
            {
                if (color1.a == color2.a) same++;
            }
        }

        return same;
    }


    void Update()
    {
#if UNITY_EDITOR

        if (Input.GetKeyDown(KeyCode.I))
        {
            SaveTextureToFileUtility.SaveRenderTextureToFile(m_Texture, $"Assets/2D/levelTextures/{nameToSave}");
        }

#endif

        if(readyToCalculate)
        {
            var result = GetCompareResult();

            successProgress = result;
        }
    }

    private float GetCompareResult()
    {
        float result = 0;

        var currentTex = GetCurrentTexture();

        var compareColors = currentTex.GetPixels(0, 0, currentTex.width, currentTex.height);

        var same = CalculateTextureDifference(compareColors);

        targetExistPercent = (targetPixelsCount / (float)initialTargetPixelCount);

        result = ((same / (float)toComparePixels) - initialPercent) / (1.0f - initialPercent) * targetExistPercent;

        return result;
    }

    private Texture2D GetCurrentTexture()
    {
        Texture2D tex;

        tex = new Texture2D(m_Texture.width, m_Texture.height, TextureFormat.ARGB32, false, false);

        var oldRt = RenderTexture.active;
        RenderTexture.active = m_Texture;
        tex.ReadPixels(new Rect(0, 0, m_Texture.width, m_Texture.height), 0, 0);

        tex.Apply();
        RenderTexture.active = oldRt;

        return tex;
    }
}
