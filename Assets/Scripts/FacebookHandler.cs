﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacebookHandler : MonoBehaviour
{
    private static FacebookHandler instance;
    private bool initialization;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        if (FB.IsInitialized == false && initialization == false)
        {
            FB.Init();
            initialization = true;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause == false)
        {
            if (FB.IsInitialized == true)
            {
                FB.ActivateApp();
            }
            else
            {
                if (!initialization)
                {
                    FB.Init();
                    initialization = true;
                }
                StartCoroutine(Activation());
            }
        }
    }

    private IEnumerator Activation()
    {
        yield return new WaitUntil(() => FB.IsInitialized == true);
        FB.ActivateApp();
    }
}
