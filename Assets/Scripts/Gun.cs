using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Gun : MonoBehaviour
{
    [SerializeField] private Tube tube;

    [SerializeField] private Waterflow flow_prefab;

    [SerializeField] private AnimationCurve recoilCurve;
    [SerializeField] private AnimationCurve fovCurve;

    [SerializeField] private float recoilRestoreSmooth, maxRecoilOffset;

    [SerializeField] private Camera cam;

    public List<GunTrigger> triggers;

    private List<Waterflow> activeFlows = new List<Waterflow>();

    private float flowDuration, releaseDuration;

    private bool flowActive, release;

    private float startZOffset;

    public int currentTrigger;

    public GunTrigger Trigger => triggers[currentTrigger];

    public ParticleSystem upgradePart;

    public List<int> prices;

    public int currentUpgradePrice => prices[currentTrigger];

    public AudioSource flowAudio, upgradeAudio;

    private void Start()
    {
    }

    void LateUpdate()
    {
        //tube.connectionSegment.position = Trigger.connectionPoint.position;
        //tube.connectionSegment.rotation = Trigger.connectionPoint.rotation;
    }

    public void UpdateGun()
    {
        if(flowActive)
        {
            flowDuration += Time.deltaTime;

            var mPos = Trigger.model.localPosition;

            var curveValue = recoilCurve.Evaluate(flowDuration);

            mPos.z = Mathf.MoveTowards(mPos.z, curveValue, 3 * Time.deltaTime);

            Trigger.model.localPosition = mPos;

            //var fovValue = fovCurve.Evaluate(flowDuration);

            //cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, Mathf.LerpUnclamped(60, 70, fovValue), .2f);
        }

        if(release)
        {
            releaseDuration += Time.deltaTime;

            var mPos = Trigger.model.localPosition;

            mPos.z = Mathf.MoveTowards(mPos.z, 0, 2 * Time.deltaTime);

            Trigger.model.localPosition = mPos;
        }

        tube.connectionSegment.position = Trigger.connectionPoint.position;
        tube.connectionSegment.rotation = Trigger.connectionPoint.rotation;
    }

    public void BeginFlow()
    {
        activeFlows.Clear();

        foreach (var item in Trigger.flowPoints)
        {
            var flow = Instantiate(flow_prefab);

            flow.transform.position = item.position;
            flow.transform.rotation = item.rotation;

            flow.transform.parent = item;

            activeFlows.Add(flow);
        }

        startZOffset = Trigger.model.localPosition.z;

        flowActive = true;
        release = false;

        flowDuration = 0;

        flowAudio.Play();
    }

    public void Stop()
    {
        Trigger.Down();
    }

    public void StopFlow()
    {
        foreach (var item in activeFlows)
        {
            item.Release();
        }

        activeFlows.Clear();

        flowActive = false;
        release = true;

        releaseDuration = 0;

        flowAudio.Stop();
    }

    public void UpdateFlows()
    {
        activeFlows.ForEach(f => f.UpdateFlow());
    }

    public void UpgradeGun()
    {
        upgradePart.Play();

        currentTrigger++;

        if (currentTrigger > triggers.Count - 1) currentTrigger = 0;

        foreach (var item in triggers)
        {
            item.model.gameObject.SetActive(false);
        }

        triggers[currentTrigger].model.gameObject.SetActive(true);

        upgradeAudio.Play();
    }
}
