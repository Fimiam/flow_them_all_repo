using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Cash2D : MonoBehaviour
{
    public SpriteRenderer sprite;

    void Start()
    {
        transform.forward = transform.position - Camera.main.transform.position;

        var seq = DOTween.Sequence();

        seq.Append(transform.DOScale(1, .22f));

        //seq.Join(transform.DOMove(transform.position + Vector3.up, .3f));

        seq.Append(sprite.DOFade(0, .25f));

        Gameplay.Instance.MoneyGot(10);
    }
}
