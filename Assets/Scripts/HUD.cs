using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] private GameObject health;

    [SerializeField] private Image healthFill;

    [SerializeField] private Gradient healthGradient;

    private void Start()
    {
        health.SetActive(false);
    }

    public void SetHealth(float fill)
    {
        health.SetActive(fill < 1.0f && fill > 0f);

        healthFill.fillAmount = fill;

        healthFill.color = healthGradient.Evaluate(fill);
    }
}
