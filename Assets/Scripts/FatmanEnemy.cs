using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FatmanEnemy : MonoBehaviour
{
    [SerializeField] private Animator animator;

    [SerializeField] private NavMeshAgent agent;

    [SerializeField] private float maxRunVelocity = 1;

    [SerializeField] private float runnigSpeed = 2;

    private int move_hash;

    // Start is called before the first frame update
    void Start()
    {
        move_hash = Animator.StringToHash("movement");

        var character = FindObjectOfType<Player>();

        var targetPos = character.transform.position;

        Debug.DrawLine(transform.position, targetPos, Color.red, 4);

        agent.SetDestination(targetPos);

        agent.speed = runnigSpeed * Random.Range(.5f, 1);
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat(move_hash, agent.velocity.magnitude / maxRunVelocity);
    }
}
