﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFlowInteractable
{
    public bool IsFlowReflective();

    public void FlowHit(float hitPercent, Vector3 hitDirection, Vector3 hitPosition);

    public void Interact(Waterflow flow);

    public Vector3 GetPosition();
}
