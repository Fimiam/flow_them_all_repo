using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ValueData : MonoBehaviour
{
    public static ValueData instance;

    public TextMeshProUGUI text;

    public float value;

    private float toFadeTime;

    public Color positiveColor, negativeColor;

    private void Awake()
    {
        instance = this;
    }

    public void AddValue(float value)
    {
        this.value += value;

        text.text = $"{(this.value > 0 ? '+' : "")}{(this.value.ToString("0.00"))}";

        text.color = this.value > 0 ? positiveColor : negativeColor;

        toFadeTime = .9f;
    }

    void Update()
    {
        if(toFadeTime > 0)
        {
            toFadeTime -= Time.deltaTime;

            var col = text.color;

            col.a = Mathf.MoveTowards(col.a, Mathf.InverseLerp(0, .3f, toFadeTime) * positiveColor.a, 4.4f * Time.deltaTime);

            text.color = col;
        }
        else
        {
            if(text.color.a == 0)
                value = 0;
        }
    }
}
