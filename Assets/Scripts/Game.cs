using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;
using GameAnalyticsSDK;
using DG.Tweening;

public class Game : MonoBehaviour
{
    public static Game instance;

    public bool soundActive;
    public bool vibroActive;

    public int sessionsCount;

    public int currentLevel;

    public bool haveInternet;

    public int premium;

    public int money;

    public bool HasPremium => premium > 0;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else if(instance != null && instance != this)
        {
            Destroy(gameObject);

            return;
        }

        GameAnalytics.Initialize();

        Application.targetFrameRate = 60;

        soundActive = PlayerPrefs.GetInt("sound", 1) > 0;
        vibroActive = PlayerPrefs.GetInt("vibro", 1) > 0;

        premium = PlayerPrefs.GetInt("premium", 0);

        sessionsCount = PlayerPrefs.GetInt("sessions");

        money = PlayerPrefs.GetInt("money", 50);

        currentLevel = PlayerPrefs.GetInt("level");
    }


    private void Start()
    {
        StartCoroutine(CheckingConnection());

        BeginSession();
    }

    internal void SetMoney(int value)
    {
        money = value;

        PlayerPrefs.SetInt("money", money);
    }

    internal void CompleteLevel()
    {
        currentLevel++;

        PlayerPrefs.SetInt("level", currentLevel);
    }

    private void BeginSession()
    {
        sessionsCount++;

        PlayerPrefs.SetInt("sessions", sessionsCount);
    }

    private string GenerateRandomName()
    {
        return "Player" + (Random.Range(0, 9999)).ToString("D4");
    }

    public void SetSound(bool value)
    {
        soundActive = value;

        PlayerPrefs.SetInt("sound", value ? 1 : 0);

        VibrationManager.instance.VibroClick();
    }

    public void SetVibro(bool value)
    {
        vibroActive = value;

        PlayerPrefs.SetInt("vibro", value ? 1 : 0);

        VibrationManager.instance.VibroClick();
    }

    public IEnumerator CheckingConnection()
    {
        while(true)
        {
            UnityWebRequest www = new UnityWebRequest("http://google.com");

            www.SendWebRequest();

            while (www.result == UnityWebRequest.Result.InProgress)
            {
                yield return null;
            }

            haveInternet = www.result == UnityWebRequest.Result.Success;

            yield return new WaitForSeconds(3);
        }
    }

    internal void SetPremium()
    {
        premium = 1;

        PlayerPrefs.SetInt("premium", 1);
    }
}

