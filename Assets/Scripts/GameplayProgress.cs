using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;

public class GameplayProgress : MonoBehaviour
{
    public TextMeshProUGUI text;

    [SerializeField] private float minWidth = 20, maxWidth = 430;

    [SerializeField] private RectTransform fillRect, viewRect;

    [SerializeField] private float progressSmooth = .34f;

    private float progress, targetProgress;

    private bool appearing;

    public float appearingVelocity, appearingFreq = 12f, appearingDump = .4f;

    //public Score score;

    public CanvasGroup bgGroup;

    [SerializeField] private float completeLevelScale = 3;

    [SerializeField] private float textScaleVelocity, textScaleFreq = 12, textScaleDump = .4f;

    private bool completeView;

    public void SetProgress(float value, bool fast = false)
    {
        value = Mathf.Clamp01(value);

        if(fast)
        {
            progress = targetProgress = value;
        }
        else
        {
            targetProgress = value;
        }
    }

    internal void LevelEndFeedback()
    {
        transform.DOShakePosition(1.5f, 23);

        VibrationManager.instance.VibroLevelEnd();
    }

    private void Update()
    {
        progress = Mathf.MoveTowards(progress, targetProgress, progressSmooth * Time.deltaTime);

        var width = Mathf.Lerp(minWidth, maxWidth, progress);

        var size = fillRect.sizeDelta;

        size.x = width;

        fillRect.sizeDelta = size;

        if(appearing)
        {
            var pos = viewRect.localPosition;

            Springing.CalcDampedSimpleHarmonicMotion
                (ref pos.y, ref appearingVelocity, 0, Time.deltaTime, appearingFreq, appearingDump);

            viewRect.localPosition = pos;

            if (Mathf.Approximately(appearingVelocity, 0.0f)) appearing = false;
        }

        var scale = text.transform.localScale.x;

        var targetScale = completeView ? completeLevelScale : 1.0f;

        if(Mathf.Abs(scale - targetScale) > float.Epsilon)
        {
            Springing.CalcDampedSimpleHarmonicMotion
                (ref scale, ref textScaleVelocity, targetScale, Time.deltaTime, textScaleFreq, textScaleDump);

            text.transform.localScale = Vector3.one * scale;
        }
    }

    public void CompleteView()
    {
        bgGroup.DOFade(0, .4f);

        completeView = true;
    }

    public void GameplayView()
    {
        bgGroup.DOFade(1, .4f);

        text.transform.DOPunchScale(Vector3.one * 1.8f, .4f);
        completeView = false;

        VibrationManager.instance.VibroClick();
    }

    internal void Show()
    {
        StartCoroutine(Showing());

    }

    private IEnumerator Showing()
    {
        yield return new WaitForSeconds(.5f);

        //score.Show();

        appearing = true;
    }

    public void ShowVictory()
    {
        bgGroup.DOFade(0, .3f);
    }
}
