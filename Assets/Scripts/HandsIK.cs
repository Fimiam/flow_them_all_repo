using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsIK : MonoBehaviour
{
    public Transform lHandTarget, rHandTarget;

    public Animator animator;

    public float weight;

    private void OnAnimatorIK(int layerIndex)
    {
        if(rHandTarget != null)
        {
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, weight);
            animator.SetIKPosition(AvatarIKGoal.RightHand, rHandTarget.position);
        }

        if(lHandTarget != null)
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, weight);
            animator.SetIKPosition(AvatarIKGoal.LeftHand, lHandTarget.position);
        }


    }

    //// Start is called before the first frame update
    //void Start()
    //{
        
    //}

    //// Update is called once per frame
    //void Update()
    //{
        
    //}
}
