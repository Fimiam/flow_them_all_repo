using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour
{
    public List<Enemy> enemies;

    bool begin;

    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0) && !begin)
        {
            begin = true;
            ActivateEnemies();
        }
    }

    public void ActivateEnemies()
    {
        enemies.ForEach(e => e.enabled = true);
    }

}
