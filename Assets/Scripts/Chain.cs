using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chain : MonoBehaviour
{
    [SerializeField] private ChainElement element_prefab;

    public int generateCount;

    public List<ChainElement> elements;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Cut(elements[Mathf.FloorToInt(elements.Count * .5f)]);
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            Vector3 lastPos = transform.position;

            for (int i = 0; i < generateCount; i++)
            {
                var element = Instantiate(element_prefab);

                element.transform.position = lastPos;

                lastPos += Vector3.up * .33f;

                if(i == 0) Destroy(element.backJoint);

                if(i > 0)
                {
                    elements[i - 1].forwardJoint.connectedBody = element.body;

                    element.backJoint.connectedBody = elements[i - 1].body;
                }

                if(i % 2 != 0)
                {
                    element.model.localRotation = Quaternion.Euler(Vector3.up * 90);
                }

                element.body.isKinematic = true;

                element.transform.parent = transform;

                elements.Add(element);
            }
        }
    }

    public void Cut(ChainElement element)
    {
        var index = elements.IndexOf(element);

        if(index < 0) return;

        if(index < elements.Count - 1)
        {
            var next = elements[index + 1];

            Destroy(next.backJoint);
        }

        if(index > 0)
        {
            var prev = elements[index - 1];

            Destroy(prev.forwardJoint);
        }

        var newChain = Instantiate(this);

        foreach (var item in newChain.elements)
        {
            Destroy(item.gameObject);
        }

        newChain.elements.Clear();

        for (int i = index + 1; i < elements.Count; i++)
        {
            var transferElement = elements[i];

            newChain.elements.Add(transferElement);

            transferElement.transform.parent = newChain.transform;
        }

        for (int i = 0; i < newChain.elements.Count + 1; i++)
        {
            elements.RemoveAt(index);
        }

        //elements.Remove(element);

        Destroy(element.gameObject);

    }
}
