using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowDrops : MonoBehaviour
{
    [SerializeField] private LayerMask wetMask;

    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;

    [SerializeField] private WaterSpot dropSpot_prefab;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

        for (int i = 0; i < numCollisionEvents; i++)
        {
            var norm = collisionEvents[i].normal;
            var pos = collisionEvents[i].intersection;

            var ray = new Ray(pos - norm * .03f, -norm);

            if (Physics.Raycast(ray, out RaycastHit hit, 99, wetMask))
            {
                Instantiate(dropSpot_prefab, pos, Quaternion.identity, null);
            }
        }
    }
}
