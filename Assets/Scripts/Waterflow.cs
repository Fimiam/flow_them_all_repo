﻿using Dreamteck.Splines;
using System.Collections.Generic;
using UnityEngine;

public class Waterflow : MonoBehaviour
{
    [SerializeField] private Waterflow flow_prefab;

    [SerializeField] private SplineComputer spline;

    [SerializeField] private GameObject hitSplashEffect;

    [SerializeField] private TubeGenerator tube;

    [SerializeField] private float minFlowSpeed = 2, maxFlowSpeed = 3, fisualFlowMult = .3f, minPushForce = 1, maxPushForce = 3;

    [SerializeField] private LayerMask hitMask;

    [SerializeField] private float minFlowT, maxFlowT, fSpeed;

    [SerializeField] private float flowDensity = 10, flowGravity = 9.81f;

    private float flowSpeed;

    private bool releasing;

    private MeshRenderer tubeRenderer;

    private double hitPercent;

    private IFlowInteractable Interactable;

    private List<SplinePoint> points = new List<SplinePoint>();

    private Waterflow subFlow;

    private Vector3 lastHitPoint;

    [SerializeField] private Transform points_prefab;

    private List<Vector3> positionPoints = new List<Vector3>();

    private float flowT, flowTStep;

    [SerializeField] private AnimationCurve flowLerpingCurve, flowSizeDuringLength, flowFillCurve, flowReleaseCurve;

    private float flowDuration, releaseDuration;

    private void Start()
    {
        SetTransform(transform.position, transform.rotation);

        flowT = maxFlowT; //Mathf.Lerp(minFlowT, maxFlowT, Vector3.Dot(Vector3.up, transform.forward) * .5f + .5f);

        flowTStep = flowT / flowDensity;

        float t = 0;

        Vector3 pos = default;

        for (int i = 0; i < flowDensity; i++)
        {
            t = i * flowTStep;

            pos = (transform.position + (transform.forward * fSpeed * t)) - Vector3.up * (flowGravity * .5f) * (t * t);

            positionPoints.Add(pos);
        }


        for (int i = 0; i < flowDensity; i++)
        {
            var sp = new SplinePoint();

            sp.color = Color.white;

            sp.size = flowSizeDuringLength.Evaluate((i / (float)flowDensity));

            sp.normal = Vector3.up;

            sp.position = positionPoints[i];

            points.Add(sp);
        }

        spline.SetPoints(points.ToArray());

        tubeRenderer = tube.GetComponent<MeshRenderer>();

        flowSpeed = Random.Range(minFlowSpeed, maxFlowSpeed);

        //for (int i = 1; i < positionPoints.Count; i++)
        //{
        //    Debug.DrawLine(positionPoints[i - 1], positionPoints[i], Color.black, 6);
        //}
    }

    public void UpdateFlow()
    {
        FlowProcess();
    }

    private void Update()
    {
        FlowReleasing();

        HitProcess();

        PushProcess();
        
        FlowVisual();
    }

    private void FlowReleasing()
    {
        if (!releasing) return;

        flowT = maxFlowT;

        flowTStep = flowT / flowDensity;

        float t = 0;

        Vector3 pos = default;

        for (int i = 0; i < flowDensity; i++)
        {
            t = i * flowTStep;

            positionPoints[i] = (transform.position + (transform.forward * fSpeed * t)) - Vector3.up * (flowGravity * .5f) * (t * t);
        }

        for (int i = 0; i < flowDensity; i++)
        {
            pos = Vector3.Lerp(spline.GetPointPosition(i), positionPoints[i], flowLerpingCurve.Evaluate((float)i / (float)flowDensity));

            spline.SetPointPosition(i, pos);
        }

        spline.RebuildImmediate();

        tube.RebuildImmediate();
    }

    public void SetTransform(Vector3 position, Quaternion rotation)
    {
        var eul = rotation.eulerAngles;

        eul.z = 0;
    }

    public void SetAsSubflow()
    {
        var mat = tube.GetComponent<Renderer>().materials[0];

        mat.SetFloat("_WaveEdge", .8f);
    }

    public void FlowProcess()
    {
        if (releasing) return;

        flowDuration += Time.deltaTime;

        flowT = maxFlowT;

        flowTStep = flowT / flowDensity;

        float t = 0;

        Vector3 pos = default;

        for (int i = 0; i < flowDensity; i++)
        {
            t = i * flowTStep;

            positionPoints[i] = (transform.position + (transform.forward * fSpeed * t)) - Vector3.up * (flowGravity * .5f) * (t * t);
        }

        for (int i = 0; i < flowDensity; i++)
        {
            pos = Vector3.Lerp(spline.GetPointPosition(i), positionPoints[i], flowLerpingCurve.Evaluate((float)i / (float)flowDensity));

            spline.SetPointPosition(i, pos);
        }

        spline.RebuildImmediate();

        tube.RebuildImmediate();
    }

    private void HitProcess()
    {
        if(spline.Raycast(out RaycastHit hit, out hitPercent, hitMask))
        {
            Interactable = hit.collider.GetComponent<IFlowInteractable>();

            if (Interactable == null)
            {
                hitPercent = 1;

                hitSplashEffect.SetActive(false);

                if (subFlow != null)
                {
                    subFlow.Release();
                    subFlow = null;
                }

                return;
            }

            lastHitPoint = hit.point;

            //Debug.Log("hit?");

            var canHit = tube.clipTo > hitPercent - .05f;

            hitSplashEffect.transform.position = hit.point;

            hitSplashEffect.transform.forward = hit.normal;

            hitSplashEffect.SetActive(canHit);

            if (!releasing && Interactable.IsFlowReflective() && canHit)
            {
                if (subFlow == null)
                {
                    subFlow = Instantiate(flow_prefab);

                    subFlow.SetAsSubflow();
                }

                var dir = Vector3.Reflect(new Vector3(transform.forward.x, 0, transform.forward.z), hit.normal);

                subFlow.SetTransform(hit.point, Quaternion.LookRotation(dir));
            }
            else
            {
                if(subFlow != null)
                {
                    subFlow.Release();
                    subFlow = null;
                }
            }
        }
        else
        {
            Interactable = null;

            hitPercent = 1;

            hitSplashEffect.SetActive(false);

            if(subFlow != null)
            {
                subFlow.Release();
                subFlow = null;
            }
        }
    }

    private void FlowVisual()
    {
        if(tube.clipTo > hitPercent)
        {
            tube.clipTo = hitPercent;
        }
        else
        {
            tube.clipTo = Mathf.MoveTowards((float)tube.clipTo, (float)hitPercent, flowSpeed * fisualFlowMult * Time.deltaTime);
        }

        if (releasing)
        {
            releaseDuration += Time.deltaTime;

            flowSpeed = Mathf.MoveTowards(flowSpeed, 0.1f, 5 * Time.deltaTime);

            transform.position += Vector3.down * 9 * Time.deltaTime;

            tube.clipFrom = Mathf.MoveTowards((float)tube.clipFrom, (float)hitPercent, flowSpeed * fisualFlowMult * Time.deltaTime);

            if (hitPercent - tube.clipFrom < .01f)
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void PushProcess()
    {
        if(tube.clipTo > hitPercent - .05f && Interactable != null)
        {
            Interactable.FlowHit((float)hitPercent, (Interactable.GetPosition() - transform.position).normalized, lastHitPoint);
        }
    }

    public void Release()
    {
        if (releasing) return;

        releasing = true;

        Debug.Log("release");

        transform.parent = null;

        if (subFlow) subFlow.Release();

        //Destroy(pointsParent.gameObject);

        Destroy(gameObject, 5);
    }
}
