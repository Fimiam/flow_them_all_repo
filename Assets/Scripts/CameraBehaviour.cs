using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField] private Transform target;

    [SerializeField] private Vector3 offset;

    [SerializeField] private float smoothPos, smoothRot;

    [SerializeField] private Vector3 lookOffset;

    void LateUpdate()
    {
        var pos = target.position + target.rotation * offset;

        transform.position = Vector3.Lerp(transform.position, pos, smoothPos);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(target.forward + lookOffset, target.up), smoothRot);
    }
}
