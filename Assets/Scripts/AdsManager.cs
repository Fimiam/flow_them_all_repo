using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    public static AdsManager instance { get; private set; }

    public string androidAppID;
    public string bannerID;
    public string interstitialID;
    public string rewardedID;

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardedAd rewardedAd;

    public bool rewardedAvailable => rewardedAd != null &&  rewardedAd.IsLoaded();

    public bool bannerShown;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else if (instance != null && instance != this)
        {
            Destroy(gameObject);

            return;
        }
    }

    private void Start()
    {
        MobileAds.Initialize(initStatus =>
        {
            RequestBanner();
            RequestInterstitial();
            //RequestRewarded();
        });     
    }

    private void RequestBanner()
    {

#if UNITY_ANDROID
        string adUnitId = bannerID;
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
            string adUnitId = "unexpected_platform";
#endif
   
        this.bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        
        ShowBanner();
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = interstitialID;
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        this.interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        this.interstitial.LoadAd(request);
    }

    private void RequestRewarded()
    {
#if UNITY_ANDROID
        string adUnitId = rewardedID;
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif

        this.rewardedAd = new RewardedAd(adUnitId);
        this.rewardedAd.OnAdFailedToLoad += (a, b) => RequestRewarded();
        this.rewardedAd.OnAdClosed += (a, b) => RequestRewarded();

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);
    }

    private void HandleRewardedAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        RequestRewarded();
    }

    public void ShowRewarded(Action<bool> callback)
    {
        if(rewardedAd.IsLoaded())
        {
            rewardedAd.OnUserEarnedReward += (a, b) => { callback?.Invoke(true); };
            rewardedAd.OnAdFailedToShow += (a, b) => { callback?.Invoke(false); };

            rewardedAd.Show();
        }
    }

    public void ShowBanner()
    {
        if (Game.instance.HasPremium || bannerShown) return;

        if (bannerView == null) RequestBanner();

        AdRequest request = new AdRequest.Builder().Build();
   
        bannerView.LoadAd(request);

        bannerShown = true;
    }

    public void HideBanner()
    {
        if(bannerView != null) bannerView.Hide();

        bannerShown = false;
    }

    public void ShowInterstitial()
    {
        if (this.interstitial.IsLoaded() && !Game.instance.HasPremium)
        {
            this.interstitial.Show();
        }
    }

    public void PremiumBought()
    {
        if(bannerView != null)
            bannerView.Hide();
    }

    private void OnDestroy()
    {
        if (interstitial != null) interstitial.Destroy();
    }

}
