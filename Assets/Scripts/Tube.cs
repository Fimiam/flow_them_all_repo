using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tube : MonoBehaviour
{
    public Transform connectionSegment;

    [SerializeField] private SplineComputer spline;

    [SerializeField] private List<Transform> points;

    private List<SplinePoint> splinePoints = new List<SplinePoint>();

    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in points)
        {
            AddPoint(item);
        }

        spline.SetPoints(splinePoints.ToArray());
    }

    
    void LateUpdate()
    {
        for (int i = 0; i < points.Count; i++)
        {
            spline.SetPointPosition(i, points[i].position);
            spline.SetPointNormal(i, points[i].up);
        }

        spline.RebuildImmediate();
    }

    private void AddPoint(Transform point)
    {
        SplinePoint sp = new SplinePoint();

        sp.position = point.position;

        sp.normal = point.up;

        sp.size = 1.0f;

        sp.color = Color.white;

        splinePoints.Add(sp);
    }
}
