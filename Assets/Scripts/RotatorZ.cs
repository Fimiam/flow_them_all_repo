using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorZ : MonoBehaviour
{
    [SerializeField] private Transform model;

    [SerializeField] private float speed = 20;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        model.Rotate(Vector3.forward * speed * Time.deltaTime);
    }
}
