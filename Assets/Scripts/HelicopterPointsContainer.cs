using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class HelicopterPointsContainer : MonoBehaviour
{
    public Vector3 GetDropPoint()
    {
        if (transform.childCount == 0) return transform.position;

        return transform.GetChild(Random.Range(0, transform.childCount)).position;
    }
}
