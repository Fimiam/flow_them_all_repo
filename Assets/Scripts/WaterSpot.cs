using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WaterSpot : MonoBehaviour
{
    [SerializeField] private Vector3 minScale, maxScale;

    [SerializeField] private Transform spotTransform;

    void Start()
    {
        spotTransform.localRotation = Quaternion.Euler(Vector3.up * Random.value * 360);

        var targetScale = new Vector3(Random.Range(minScale.x, maxScale.x), Random.Range(minScale.y, maxScale.y), Random.Range(minScale.z, maxScale.z));

        spotTransform.localScale = Vector3.zero;

        spotTransform.DOScale(targetScale, Random.Range(.1f, .2f));
    }
}
