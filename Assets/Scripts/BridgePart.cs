using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BridgePart : MonoBehaviour, IFlowInteractable
{
    [SerializeField] private float flowForce = 1;

    private new Rigidbody rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }


    public void FlowHit(float hitPercent, Vector3 hitDirection, Vector3 hitPosition)
    {
        rigidbody.AddForce(hitDirection * flowForce, ForceMode.Impulse);
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void Interact(Waterflow flow)
    {
        
    }

    public bool IsFlowReflective()
    {
        return false;
    }
}
