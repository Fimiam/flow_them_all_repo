using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunTrigger : MonoBehaviour
{
    [SerializeField] private Gun gun;
    [SerializeField] private new Animation animation;

    [SerializeField] private string upAnimName, downAnimName;

    public Transform model, connectionPoint;
    public List<Transform> flowPoints;

    public void TriggerUp()
    {
        gun.BeginFlow();
    }

    public void Up()
    {
        animation.Play(upAnimName);
    }

    public void Down()
    {
        animation.Play(downAnimName);
    }

    public void TriggerDown()
    {
        gun.StopFlow();
    }
}
