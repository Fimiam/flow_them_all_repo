using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainElement : MonoBehaviour
{
    public Rigidbody body;

    public HingeJoint forwardJoint, backJoint;

    public Transform model;
}
