using UnityEngine;
public class ColbLiquid : MonoBehaviour
{
    private readonly int WOBBLE_X = Shader.PropertyToID("_WobbleX");
    private readonly int WOBBLE_Z = Shader.PropertyToID("_WobbleZ");
    private readonly int FILL_AMOUNT = Shader.PropertyToID("_FillAmount");

    [SerializeField] private float emptyAmount, fullAmount;

    [SerializeField] private Renderer rend;

    public float MaxWobble = 0.03f;
    public float WobbleSpeed = 5.0f;
    public float RecoveryRate = 1f;

    Vector3 prevPos;
    Vector3 prevRot;
    float wobbleAmountToAddX, targetWobbleX;
    float wobbleAmountToAddZ, targetWobbleZ;

    private Material matToChange;

    private void Start()
    {
        matToChange = rend.materials[0];
    }


    public void SetFill(float amount = 1)
    {
        rend.materials[0].SetFloat(FILL_AMOUNT,Mathf.Lerp(emptyAmount, fullAmount, amount)/* - transform.position.y*/);
    }

    private void Update()
    {
        //SetFill(.5f);

        // 1. decreases the wobble over time
        wobbleAmountToAddX = Mathf.Lerp(wobbleAmountToAddX, 0, Time.deltaTime * RecoveryRate);
        wobbleAmountToAddZ = Mathf.Lerp(wobbleAmountToAddZ, 0, Time.deltaTime * RecoveryRate);



        // 2.make a sine wave of the decreasing wobble
        float wobbleAmountX = wobbleAmountToAddX * Mathf.Sin(WobbleSpeed * Time.time);
        float wobbleAmountZ = wobbleAmountToAddZ * Mathf.Sin(WobbleSpeed * Time.time);



        // 3.send it to the shader
        rend.materials[0].SetFloat(WOBBLE_X, wobbleAmountX);
        rend.materials[0].SetFloat(WOBBLE_Z, wobbleAmountZ);

        // 4.Move Speed
        Vector3 moveSpeed = (prevPos - transform.position) / Time.deltaTime;
        Vector3 rotationDelta = transform.rotation.eulerAngles - prevRot;

        // 5.add clamped speed to wobble
        wobbleAmountToAddX += Mathf.Clamp((moveSpeed.x + (rotationDelta.z * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);
        wobbleAmountToAddZ += Mathf.Clamp((moveSpeed.z + (rotationDelta.x * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);

        wobbleAmountToAddX = Mathf.Lerp(wobbleAmountToAddX, targetWobbleX, 3 * Time.deltaTime);
        wobbleAmountToAddZ = Mathf.Lerp(wobbleAmountToAddZ, targetWobbleZ, 3 * Time.deltaTime);

        // 6.save the last position
        prevPos = transform.position;
        prevRot = transform.rotation.eulerAngles;
    }
}
