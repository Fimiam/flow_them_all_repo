using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Vector3 gunOffset;

    [SerializeField] private float horisontalSens, gunSmooth;

    [SerializeField] private Transform gunTarget;

    [SerializeField] private Gun gun;

    private CameraSpaceInput input;

    private bool tap;

    
    void Start()
    {
        input = new CameraSpaceInput();
    }

    
    void Update()
    {
        input.Update();

        transform.Rotate(Vector3.up, horisontalSens * input.x * Time.deltaTime, Space.Self);
        transform.Rotate(Vector3.right, horisontalSens * -input.y * Time.deltaTime, Space.Self);

        var r = transform.rotation.eulerAngles;

        r.z = 0;

        transform.rotation = Quaternion.Euler(r);

        var gunPos = transform.position + transform.rotation * gunOffset;

        gun.transform.position = Vector3.Lerp(gun.transform.position, gunPos, gunSmooth);

        gun.transform.LookAt(gunTarget);

        if(!tap && input.hold)
        {

            gun.Trigger.Up();

            //gun.BeginFlow();

            tap = true;
        }

        if(!input.hold)
        {
            if(tap)
            {

                gun.Stop();
            }

            tap = false;
        }

        gun.UpdateGun();
        gun.UpdateFlows();
    }
}
