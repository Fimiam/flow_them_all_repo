using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance { get; private set; }

    public int progressionLevel, levelIndex;

    public List<ParticleSystem> completeEffects;

    public bool complete;

    //public ProgressUI ropeProgress;

    //public List<Level> level_prefabs;

    //public Level currentLevel;

    [SerializeField] private RectTransform completeText;

    public RectTransform restartButton, activateButton, failRect;

    public Control control;
    public AnimationCurve failAppearCurve;

    public Image failFade_Image;

    public float maxRopeLength, ropeLength;

    public GameplayProgress progressView;

    public List<Texture2D> levelsProjector;
    public List<Texture2D> levelsTarget;

    //public int enemiesToKill = 20;

    //private int enemiesKilled;

    public TextMeshProUGUI moneyText, upgradePriceText;

    public Gun gun;

    public CanvasGroup upgradeCanvasGroup;

    public RectTransform upgradeRect;

    public TextMeshProUGUI bearText, comboText;

    public int bearPercent = 50;

    public Animator putinFaceAnimator;

    public int killed = 0;

    public AudioSource laughSFX;

    private void Awake()
    {
        Instance = this;

        progressionLevel = PlayerPrefs.GetInt("progression", 1);

        levelIndex = PlayerPrefs.GetInt("level");

        Application.targetFrameRate = 60;

    }

    private void Start()
    {
        SpawnLevel();

        Game.instance.SetMoney(100);

        moneyText.text = $"{Game.instance.money}";

        upgradePriceText.text = $"{gun.currentUpgradePrice}";

        bearText.text = $"{bearPercent}%";
    }

    public void AddBearProgress(int val)
    {
        bearPercent += val;

        bearText.text = $"{bearPercent}%";

        bearText.transform.DOComplete();

        bearText.transform.DOPunchScale(Vector3.one * .2f, .2f, 1);
    }

    private IEnumerator StartingLevelRoutine()
    {
        yield return null;
    }

    public void SpawnLevel()
    {
        progressView.text.text = $"{progressionLevel}";

        //if(currentLevel == null)
        //    currentLevel = Instantiate(level_prefabs[levelIndex]);

        //maxRopeLength = currentLevel.maxRopeLength;

        //StartCoroutine(StartingLevelRoutine());
    }

    public void UpgradeButtonClick()
    {
        Debug.Log("upgradeClick");


        var money = Game.instance.money;

        Game.instance.SetMoney(money - gun.currentUpgradePrice);

        moneyText.text = $"{Game.instance.money}";

        gun.UpgradeGun();

        RefreshUpdateButton();
    }

    private void Update()
    {
#if UNITY_EDITOR

        if(Input.GetKeyDown(KeyCode.W))
        {
            LevelComplete();
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            LevelFail();
        }

#endif
    }

    public void EnemyKilled()
    {
        killed++;

        if(killed % 5 == 0)
        {
            putinFaceAnimator.SetTrigger("laugh");

            comboText.text = $"COMBO X{killed}";

            comboText.transform.parent.gameObject.SetActive(true);

            StartCoroutine(ComboText());

            laughSFX.Play();
        }

        //enemiesKilled++;

        //progressView.SetProgress(enemiesKilled / (float)enemiesToKill);

        //if(enemiesKilled >= enemiesToKill)
        //{
        //    LevelComplete();
        //}
    }

    private IEnumerator ComboText()
    {
        yield return new WaitForSeconds(2f);

        comboText.transform.parent.gameObject.SetActive(false);
    }

    public void MoneyGot(int value)
    {
        var money = Game.instance.money;

        Game.instance.SetMoney(money + value);

        moneyText.text = $"{Game.instance.money}";

        RefreshUpdateButton();
    }

    private void RefreshUpdateButton()
    {
        upgradePriceText.text = $"{gun.currentUpgradePrice}";

        if (Game.instance.money >= gun.currentUpgradePrice)
        {
            upgradeCanvasGroup.alpha = 1.0f;
            upgradeRect.localScale = Vector3.one * 1.5f;
        }
        else
        {
            upgradeCanvasGroup.alpha = .5f;
            upgradeRect.localScale = Vector3.one;
        }
    }

    public void LevelFail()
    {
        if (complete) return;

        complete = true;

        StartCoroutine(FailRoutine());
    }

    private IEnumerator FailRoutine()
    {
        //failFade_Image.gameObject.SetActive(true);

        failRect.gameObject.SetActive(true);

        failRect.DOPunchScale(Vector3.one * .3f, 2f, 4);

        //failFade_Image.DOFade(.2f, .3f);

        //failRect.DOScale(1, .6f).SetEase(failAppearCurve);

        yield return new WaitForSeconds(2f);

        RestartRequest();
    }

    public void LevelComplete()
    {
        if (complete) return;

        complete = true;

        progressionLevel++;

        PlayerPrefs.SetInt("progression", progressionLevel);

        levelIndex++;

        if (levelIndex > levelsTarget.Count - 1) levelIndex = 0;

        PlayerPrefs.SetInt("level", levelIndex);

        StartCoroutine(CompleteRoutine());
    }

    private IEnumerator CompleteRoutine()
    {
        yield return new WaitForSeconds(.1f);

        completeText.gameObject.SetActive(true);

        completeText.DOPunchScale(Vector3.one * .3f, 2f, 4);

        StartCoroutine(CompleteVFXRoutine());

        yield return new WaitForSeconds(.6f);

        AdsManager.instance.ShowInterstitial();

        yield return new WaitForSeconds(1.2f);

        RestartRequest();
    }

    private IEnumerator CompleteVFXRoutine()
    {
        while(true)
        {
            //foreach(ParticleSystem p in completeEffects)
            //{
                //p.Play();

                yield return new WaitForSeconds(Random.Range(.1f, .3f));
            //}
        }
    }

    public void RestartRequest()
    {
        SceneManager.LoadScene(0);
    }
}
