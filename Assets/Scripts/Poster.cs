using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poster : MonoBehaviour
{
    public Transform leftHoldPoint, rightHoldPoint;

    public HingeJoint joint;

    [SerializeField] private List<Material> materials = new List<Material>();

    [SerializeField] private Renderer visualRenderer;

    void Start()
    {
        visualRenderer.material = materials[Random.Range(0, materials.Count)];
    }
}
