using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleInteractObj : MonoBehaviour, IFlowInteractable
{
    [SerializeField] private new Rigidbody rigidbody;

    [SerializeField] private float hitForce = .4f;

    public void FlowHit(float hitPercent, Vector3 hitDirection, Vector3 hitPosition)
    {
        if(rigidbody != null)
            rigidbody.AddForce(hitDirection * hitForce, ForceMode.Impulse);
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void Interact(Waterflow flow)
    {
        
    }

    public bool IsFlowReflective()
    {
        return false;
    }

}
