using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureScroller : MonoBehaviour
{
    [SerializeField] private MeshRenderer mesh;

    [SerializeField] private float scrollSpeed = 200;

    // Update is called once per frame
    void Update()
    {
        var val = mesh.material.GetTextureOffset("_MainTex");

        val += Vector2.up * scrollSpeed * Time.deltaTime;

        mesh.material.SetTextureOffset("_MainTex", val);
    }
}
