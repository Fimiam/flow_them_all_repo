using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bridge : MonoBehaviour
{
    [SerializeField] private NavMeshSurface navMeshSurface;


    void Update()
    {
        navMeshSurface.BuildNavMesh();
    }
}
