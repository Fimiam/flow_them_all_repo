using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cash : MonoBehaviour
{
    public Rigidbody body;

    public List<Transform> bills;

    public ParticleSystem disappearingEffect_prefab;

    
    
    void Start()
    {
        var toRemove = Random.Range(0, 3);

        for (int i = 0; i < bills.Count; i++)
        {
            if(toRemove > i)
            {
                bills[i].gameObject.SetActive(false);
            }
            else
            {
                bills[i].localRotation = Quaternion.Euler(Vector3.up * Random.value * 360);
            }

        }

        StartCoroutine(PoofRoutine());
    }

    private IEnumerator PoofRoutine()
    {
        var delay = Random.Range(1f, 2f);

        yield return new WaitForSeconds(delay);

        var effect = Instantiate(disappearingEffect_prefab);

        effect.transform.position = transform.position;

        effect.Play();

        transform.GetChild(0).gameObject.SetActive(false);

        Gameplay.Instance.MoneyGot(10);
    }
}
