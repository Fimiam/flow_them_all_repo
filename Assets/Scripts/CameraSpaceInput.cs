﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSpaceInput
{
    public Vector3 lastInput, inputDelta;

    public Camera cam;

    public bool hold;

    public float y => inputDelta.y;
    public float x => inputDelta.x;

    public CameraSpaceInput(Camera cam)
    {
        this.cam = cam;
    }

    public CameraSpaceInput()
    {
        cam = Camera.main;
    }

    public void Update()
    {
        if (cam == null)
        {
            Debug.LogError("input has no camera");

            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            lastInput = cam.transform.InverseTransformPoint(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1)));

            hold = true;
        }

        if(Input.GetMouseButtonUp(0))
        {
            hold = false;
        }

        if (hold)
        {
            var input = cam.transform.InverseTransformPoint(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1)));

            inputDelta = input - lastInput;

            lastInput = input;
        }
        else
        {
            inputDelta = Vector3.zero;
        }

    }
}
