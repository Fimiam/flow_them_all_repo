using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterBody : MonoBehaviour, IFlowInteractable
{
    [SerializeField] private float flowForce = 1;
    private new Rigidbody rigidbody;

    public float health = 100;

    private Component joint;

    private bool isDead;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        joint = GetComponent<Joint>();
    }

    public void FlowHit(float hitPercent, Vector3 hitDirection, Vector3 hitPosition)
    {
        rigidbody.AddForceAtPosition(hitDirection * flowForce, hitPosition);

        health -= .2f;

        if(health < 0 && !isDead)
        {
            isDead = true;

            Destroy(joint);
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void Interact(Waterflow flow)
    {
        
    }

    public bool IsFlowReflective()
    {
        return false;
    }
}
