using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public static Control instance { get; private set; }

    public Vector2 input;

    private Vector3 lastPoint;

    private void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            lastPoint = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
        }

        if(Input.GetMouseButton(0))
        {
            var point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));

            input = point - lastPoint;

            lastPoint = point;
        }
        else
        {
            input = Vector2.zero;
        }
    }
}
